'use strict';

const familyRegistrations = {};
let indexCounter = 0;

document.addEventListener('DOMContentLoaded', function ready () {
  const $form = $('form');
  const $addButton = $('.add');

  $addButton.on('click', (event) => addRegistration(event, $form));
  $form.on('submit', submitForm);
}, false);

// Event Actions ---------------------------------------------------------------
function addRegistration (event, $form) {
  event.preventDefault(); // Prevent the Form being submitted

  const age = $form.find('input')
  const relationship = $form.find('select')
  const smoker = $form.find('input', 1).isChecked();

  if (!age.validate(true) || !relationship.validate()) {
    return;
  }

  const id = `list-item-${indexCounter}`;

  // increment counter
  indexCounter += 1;

  // Add Registation to familyRegistrations object
  familyRegistrations[id] = {
    age: age.val(),
    relationship: relationship.val(),
    smoker: smoker,
  };

  // Create Container
  const listItem = new Element('li')
    .addClass('registrations')
    .addId(id)
    .html(`
      <ul>
        <li>Age: ${age.val()}</li>
        <li>Relationship: ${relationship.val()}</li>
        <li>Smoker: ${smoker ? 'Yes' : 'No'}</li>
      </ul>
    `);

  // Create Delete Button
  const deleteButton = new Element('button')
    .text('Delete')
    .on('click', () => listItem.destroy());

  // Add delete to listItem
  listItem.append(deleteButton);

  // Add html to the DOM
  $('.hospital').append(listItem);

  // Clear Fields
  $form.find('input').clear()
  $form.find('select').clear()
  $form.find('input', 1).resettCheckbox()
}

function submitForm (event) {
  event.preventDefault(); // Prevent the Form being submitted

  // Convert the Object to an Array
  const values = Object.values(familyRegistrations);

  $('.debug')
    .html(JSON.stringify(values))
    .show();
}

function $ (selector) {
  let element;

  if (selector.charAt(0) === '.') {
    element = new Element(null, document.getElementsByClassName(selector.slice(1))[0]);
  } else {
    element = new Element(null, document.getElementsByTagName(selector)[0]);
  }

  element.find = function (query, index = 0) {
    return Object.assign(this.element.querySelectorAll(query)[index], this);
  }

  element.val = function () {
    return this.value;
  }

  element.clear = function () {
    return this.value = '';
  }

  element.resettCheckbox = function () {
    return this.checked = false;
  }

  element.isChecked = function () {
    return this.checked;
  }

  element.show = function () {
    this.element.style.display = 'block';

    return this;
  }

  element.validate = function (isNumber = false) {
    const value = this.val();

    if (!value) {
      console.log(`${this.name} is required`);
      return false;
    }

    if (isNumber) {
      if (isNaN(value)) {
        console.log('Please enter a Valid number.')
        return false;
      } else if (Number(value) < 1) {
        console.log('Please enter an number above 0.')
        return false;
      }
    }

    return true;
  }

  return element;
}

class Element {
  constructor (tag, element) {
    this.element = tag ? document.createElement(tag) : element;
  }

  addClass (className) {
    this.element.className = className;

    return this;
  }

  addId (id) {
    this.element.id = id;

    return this;
  }

  text (text) {
    this.element.appendChild(document.createTextNode(text));

    return this;
  }

  html (html) {
    this.element.innerHTML = html

    return this;
  }

  on (event, fn) {
    this.element.addEventListener(event, fn);

    return this;
  }

  append (child) {
    this.element.appendChild(child.element);

    return this;
  }

  destroy () {
    // Remove the registration for the DOM
    this.element.parentElement.removeChild(this.element);

    // Remove from familyRegistrations object
    delete familyRegistrations[this.element.id];
  }
}
